#!/bin/bash

while getopts "riap" OPTION
do
	case $OPTION in
    r)
      rebuild=true
      ;;
		i)
      imageins=true
			;;
		a)
	    imageins21=true
			;;
		p)
			push=true
			;;
		\?)
			echo "Used for the help menu"
			exit
			;;
	esac
done

if [ "$rebuild" = true ] ; then
  echo "Rebuild flag set"
  echo "Cleaning Up Old Containers and Remove EasyAvi Image"
  docker-compose down --rmi local
  #cleanup=true
else
  echo "Cleaning Up Old Containers"
  docker-compose down
fi

echo "Brining Up New Containers in Detached Mode"
docker-compose up -d
echo "Inserting Default Password"
docker exec mongo mongo localhost:27017/easyavi --eval 'db.users.insertOne({ login: "admin", password: "'71d04851ef2cce98797a206c896ee6ca'", type: "admin"})' > /dev/null
if [ $? = 0 ]
then
  echo "Password Inserted Successfully!"
else
  echo "Couldn't Insert Password!"
fi

if [ "$imageins" = true ] ; then
  echo "Inserting Image Collection for 20.1.6"
  echo "Make sure the OVA already exists!"
  docker exec mongo mongo localhost:27017/easyavi --eval 'db.images.insertOne({ version: "20.1.6", status: "DOWNLOADED", hash: "", progress: 100})' > /dev/null
  if [ $? = 0 ]
  then
    echo "Image Collection Inserted Successfully!"
    else
        echo "Couldn't Insert Image Collection!"
  fi
fi


if [ "$imageins21" = true ] ; then
  echo "Inserting Image Collections for 20.1.6 and 21.1.1"
  echo "Make sure the OVA already exists!"
	docker exec mongo mongo localhost:27017/easyavi --eval 'db.images.insertOne({ version: "20.1.6", status: "DOWNLOADED", hash: "", progress: 100})' > /dev/null
  if [ $? = 0 ]
  then
    echo "Image Collection Inserted Successfully For 20.1.6!"
    else
        echo "Couldn't Insert Image Collection!"
  fi
  docker exec mongo mongo localhost:27017/easyavi --eval 'db.images.insertOne({ version: "21.1.1", status: "DOWNLOADED", hash: "", progress: 100})' > /dev/null
  if [ $? = 0 ]
  then
    echo "Image Collection Inserted Successfully For 21.1.1!"
  else
    echo "Couldn't Insert Image Collection!"
  fi
fi


if [ "$push" = true ] ; then
  echo "Pushing image to personal docker hub"
  docker push charliegut14/easyavi:latest
	if [ $? = 0 ]
	then
		echo "Container Image Pushed Successfully!"
	else
		echo "Error Pushing Container Image!"
	fi
fi




#if [ "$cleanup" = true ] ; then
#  echo "Brining Up New Containers in Detached Mode"
#  docker-compose up -d
  #Bootstrap Password
#  echo "Inserting Default Password"
#  docker exec mongo mongo localhost:27017/easyavi --eval 'db.users.insertOne({ login: "admin", password: "'71d04851ef2cce98797a206c896ee6ca'", type: "admin"})' > /dev/null
#  if [ $? = 0 ]
#  then
#    echo "Password Inserted Successfully!"
#  else
#    echo "Couldn't Insert Password!"
#  fi
#else
#  echo "Cleaning Up Old Containers"
#  docker-compose down
#  echo "Brining Up New Containers in Detached Mode"
#  docker-compose up -d
  #Bootstrap Password


#elif [ "$imageins" = true ] ; then
#  echo "Image flag set"
#else
#  echo "no flag set"
#fi
